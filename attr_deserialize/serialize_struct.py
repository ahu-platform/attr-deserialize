"""Methods to serialize tree objects and dataclasses containing trees."""
from typing import Callable, List, Mapping, Union

from anytree import AnyNode
from anytree.exporter import DictExporter
import attr


def _serialize_tree(tree: AnyNode, child_iterator: Callable = list) -> dict:
    """
    Export a tree object containing attr dataclasses in its nodes
    to JSON-serializable dict.

    An optional `child_iterator` can be used for sorting and/or filtering:
    `child_iterator = lambda children: [ch for ch in children if "0" in ch.a]`
    """

    def _custom_export(attrs):
        """
        Custom exporter for tree nodes, using `attr.asdict` internally for
        attr data classes to proper JSON serialization.
        """
        data = dict(attrs)
        for field, value in data.items():
            if hasattr(value, "__attrs_attrs__"):
                data[field] = attr.asdict(
                    data[field], filter=lambda key, v: v is not None
                )

        return [(k, v) for k, v in data.items()]

    exporter = DictExporter(attriter=_custom_export, childiter=child_iterator)
    return exporter.export(tree)


def serialize_struct(
    data_instance: Union[attr.dataclass, AnyNode],
    tree_child_iterator: Callable = list,
) -> dict:
    """
    Export a dataclass with nested `anytree` objects to JSON-compatible dict.

    It just applies `attr.asdict()` to the dataclass and after that it walks
    recursively inside the generated data to convert to dict any
    included tree object, **and inside these**, it also applies `attr.asdict()`
    to any dataclass field attached to the tree object nodes, if present.

    `tree_child_iterator`, if given, it's used to sort/filter the
    children nodes in the AnyNode tree objects encountered in serialization.
    """

    def _serialize_internal_trees(data: Union[Mapping, List]):
        iterator = (
            data.items() if isinstance(data, Mapping) else enumerate(data)
        )
        for idx, value in iterator:
            if issubclass(type(value), AnyNode):
                data[idx] = _serialize_tree(value, tree_child_iterator)
            elif isinstance(value, Mapping) or isinstance(value, List):
                _serialize_internal_trees(value)

    if isinstance(data_instance, AnyNode):
        return _serialize_tree(data_instance, tree_child_iterator)

    dict_data = attr.asdict(data_instance)
    _serialize_internal_trees(dict_data)
    return dict_data
