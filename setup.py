from setuptools import setup

install_requires = ("attrs>=19.0,<20.0", "anytree>=2.4.3")

packages = {"attr_deserialize": "attr_deserialize"}

setup(
    name="attr_deserialize",
    version="0.1",
    packages=packages.keys(),
    package_dir=packages,
    url="https://gitlab.com/ahu-platform/attr-deserialize",
    license="",
    author="TBE Team",
    author_email="tbe-team@remak-dva.com",
    description="The method to de-serialize dict to attrs data class",
    include_package_data=True,
    install_requires=install_requires,
    extras_require={
        "dev": [
            "pytest==4.2.0",
            "pytest-cov==2.6.1",
            "pre-commit==1.10.3",
            "PyYAML==3.13",
        ]
    },
)
