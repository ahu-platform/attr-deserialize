# attr-deserialize

De-serialize a raw data dict in data-class (with nested data-classes).

This de-serialization is made by exploring the typing info of the
main data-class and walking the nested tree, trying all possible
combinations while expecting only one good result.

For this approach to work properly, when defining data-classes typing
**must be detailed**, in order to restrain possibilities adequately.
This approach is able to deserialize complex combinations of nested data,
like these:

```
@attr.s
class NestedItem:
    block_1: Union[PHexDisp, List[Union[PHexComponent, Filter, Fan]]]
    sup: List[Union[FanDisp, FilterDisp]]
    components: Dict[str, Union[PHexComponent, Filter, Fan]]
    panels: List[List[FilterPanelSize]]
```

To install develop requirements use [dev] flag.
```bash
pip install -e .[dev]
```
